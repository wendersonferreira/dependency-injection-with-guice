package br.com.trustsystems.ioc.test;

import br.com.trustsystems.ioc.part3.MessageService;

public class MockMessageService implements MessageService{
    public String subject;
    public String message;

    public void sendMessage(final String subject, final String message) {
        this.subject = subject;
        this.message = message;
    }
}
