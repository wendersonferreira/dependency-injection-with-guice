package br.com.trustsystems.ioc.part4;

public interface WritingService {
    void write(String message);
}