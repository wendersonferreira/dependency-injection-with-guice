package br.com.trustsystems.ioc.part4;

public interface MessageService {
    void sendMessage(String subject, String message);
}
