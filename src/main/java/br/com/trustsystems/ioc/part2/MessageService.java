package br.com.trustsystems.ioc.part2;

public interface MessageService {
    void sendMessage(String subject, String message);
}
