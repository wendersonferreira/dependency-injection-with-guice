package br.com.trustsystems.ioc.part3;

public interface MessageService {
    void sendMessage(String subject, String message);
}
